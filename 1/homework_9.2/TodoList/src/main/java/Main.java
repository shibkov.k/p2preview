import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Main {
    private static TodoList todoList = new TodoList();

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        while (true) {
            String input = scanner.nextLine();
            if (input.equals("0")) {
                break;
            }
            if (input.isEmpty()) {
                continue;
            }

            int index = -1;
            String[] splitInput = input.split(" ");
            String command = splitInput[0];
            String textTodo = "";
            if (splitInput.length > 1) {
                Pattern pattern = Pattern.compile("([^0-9])|(^\\s*$)");
                Matcher matcher = pattern.matcher(splitInput[1]);
                textTodo = input.substring(input.indexOf(" ")).trim();
                if (!matcher.find()) {
                    index = Integer.parseInt(splitInput[1]);
                }
                if (splitInput.length == 2 && index != -1) {
                    textTodo = "";
                }
                if (splitInput.length > 2 && index != -1) {
                    textTodo = textTodo.substring(textTodo.indexOf(" ")).trim();
                }
            }
            switcherClass(command, index, textTodo);
        }
    }

    private static void switcherClass(String command, int index, String textTodo) {
        switch (command) {
            case ("LIST"):
                todoList.getTodos();
                break;
            case ("ADD"):
                if (index >= 0) {
                    todoList.add(index, textTodo);
                } else {
                    todoList.add(textTodo);
                }
                break;
            case ("EDIT"):
                todoList.edit(textTodo, index);
                break;
            case ("DELETE"):
                todoList.delete(index);
                break;
            default:
                System.out.println("Неправильный ввод");
        }
    }
}
