import java.util.ArrayList;

public class TodoList {
    ArrayList<String> list;

    public TodoList() {
        list = new ArrayList<>();
    }

    public void add(String todo) {
        if (!todo.equals("")) {
            list.add(todo);
            System.out.println("Добавлено дело " + "\"" + todo + "\"");
        } else {
            System.out.println("Пустой ввод недопустим");
        }
    }

    public void add(int index, String todo) {
        //  проверьте возможность добавления
        if (!todo.equals("")) {
            if (index >= 0 && index < list.size()) {
                list.add(index, todo);
            } else {
                list.add(todo);
            }
            System.out.println("Добавлено дело " + "\"" + todo + "\"");
        } else {
            System.out.println("Пустой ввод недопустим");
        }
    }

    public void edit(String todo, int index) {
        //  проверьте возможность изменения
        if (index >= 0 && index < list.size()) {
            String removeTodo = list.get(index);
            list.remove(index);
            list.add(index, todo);
            System.out.println("Дело " + "\"" + removeTodo + "\"" + " заменено на " + "\"" + todo + "\"");
        } else {
            System.out.println("Дело с таким номером не существует");
        }
    }

    public void delete(int index) {
        //  проверьте возможность удаления дела
        if (index >= 0 && index < list.size()) {
            String removeTodo = list.get(index);
            list.remove(index);
            System.out.println("Дело " + "\"" + removeTodo + "\"" + " удалено");
        } else {
            System.out.println("Дело с таким номером не существует");
        }
    }

    public ArrayList<String> getTodos() {
        for (int i = 0; i < list.size(); i++) {
            System.out.println(i + " - " + list.get(i));
        }
        return list;
    }

}