public class Hospital {

    public static float[] generatePatientsTemperatures(int patientsCount) {

        float[] patientTemperatures = new float[patientsCount];
        for (int i = 0; i < patientTemperatures.length; i++) {
            patientTemperatures[i] = (float) (32 + (Math.random() * 8));
        }
        return patientTemperatures;
    }

    public static String getReport(float[] temperatureData) {
        int healthyPatients = 0;
        float sumTemperature = 0.f;
        StringBuilder builder = new StringBuilder();
        for (int i = 0; i < temperatureData.length; i++) {
            float temp = (float) Math.round(temperatureData[i] * 100) / 100;
            builder.append(temp);
            if (i != temperatureData.length - 1) {
                builder.append(" ");
            }
            sumTemperature += temperatureData[i];
            if (temp >= 36.2 && temp <= 37.0) {
                healthyPatients++;
            }
        }

        float midTemp = (float) (Math.round((sumTemperature / (float) temperatureData.length) * 100)) / 100;
        String report =
                "Температуры пациентов: " + builder +
                        "\nСредняя температура: " + midTemp +
                        "\nКоличество здоровых: " + healthyPatients;

        return report;
    }
}
