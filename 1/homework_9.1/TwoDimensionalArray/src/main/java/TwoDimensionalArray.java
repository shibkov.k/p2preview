public class TwoDimensionalArray {
    public static char symbol = 'X';

    public static char[][] getTwoDimensionalArray(int size) {

        // массив должен содержать символ symbol по диагоналям, пример для size = 3
        // [X,  , X]
        // [ , X,  ]
        // [X,  , X]
        char[][] arr = new char[size][size];
        for (int i = 0; i < size; i++) {
            for (int j = 0; j < size; j++) {
                arr[i][j] = ' ';
                if (i == j) {
                    arr[i][j] = symbol;
                }
                if (j == size - 1 - i) {
                    arr[i][size - 1 - i] = symbol;
                }
            }
        }
        for (char[] chars : arr) {
            System.out.println(chars);
        }
        return arr;
    }
}
