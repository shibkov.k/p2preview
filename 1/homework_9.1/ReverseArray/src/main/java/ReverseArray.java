public class ReverseArray {

    public static String[] reverse(String[] strings) {
        String temp;
        for (int i = 0; i < strings.length / 2; i++) {
            temp = strings[i];
            strings[i] = strings[strings.length - 1 - i];
            strings[strings.length - 1 - i] = temp;
        }
        return strings;
    }
}
