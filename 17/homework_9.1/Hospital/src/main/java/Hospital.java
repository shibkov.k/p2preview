
public class Hospital {

  public static float[] generatePatientsTemperatures(int patientsCount) {
    float[] patientsTemperatures = new float[patientsCount];
    for (int i = 0; i < patientsTemperatures.length; i++) {
      patientsTemperatures[i] = (float) (32 + (double) (8 * Math.random()));
    }
    return patientsTemperatures;
  }

  public static String getReport(float[] temperatureData) {
    double minTemperature = 36.2;
    double maxTemperature = 36.9;
    String temperatures = "";
    double sum = 0.0;
    int count = 0;

    for (int i = 0; i < temperatureData.length; i++) {
      temperatures += Math.round(temperatureData[i] * 10.0) / 10.0 + " ";
      sum += temperatureData[i];
      if (Math.round(temperatureData[i] * 10.0) / 10.0 >= minTemperature
          && Math.round(temperatureData[i] * 10.0) / 10.0 <= maxTemperature) {
        count++;
      }

    }
    sum = sum / temperatureData.length;
    String averageTemperature = String.format("%.2f", sum);

    String report =
        "Температуры пациентов: " + temperatures.trim() +
            "\nСредняя температура: " + averageTemperature +
            "\nКоличество здоровых: " + count;

    return report;
  }
}
