public class TwoDimensionalArray {

  public static char symbol = 'X';

  public static char[][] getTwoDimensionalArray(int size) {
    char[][] test = new char[size][size];
    int i = 0;
    int j = 0;

    for (i = 0; i < test.length; i++) {
      for (j = 0; j < test.length; j++) {
        if (i == j || i + j == size - 1) {
          test[i][j] = symbol;
        } else {
          test[i][j] = ' ';
        }
      }
    }
    return test;
  }
}
//TODO: Написать метод, который создаст двумерный массив char заданного размера.
// массив должен содержать символ symbol по диагоналям, пример для size = 3
// [X,  , X]
// [ , X,  ]
// [X,  , X]