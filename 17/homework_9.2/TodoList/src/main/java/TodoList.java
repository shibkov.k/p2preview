import java.util.ArrayList;

public class TodoList {

  ArrayList<String> list = new ArrayList<>();

  public void add(String todo) {
    list.add(todo);
    System.out.println("Добавлено дело: " + todo);
  }

  public void add(int index, String todo) {
    if (list.size() < index) {
      System.out.println("Невозможно добавить дело на указанное место. Дело добавлено в конец списка.");
      list.add(todo);
    } else {
      list.add(index, todo);
      System.out.println("Дело " + list.get(index) + "добавлено на указанное место");
    }
  }

  public void edit(String todo, int index) {
    if (list.size() <= index) {
      System.out.println("Ошибка. Дело с таким номером не существует");
    } else {
      System.out.println("Дело " + list.get(index));
      list.set(index, todo);
      System.out.println("заменено на " + list.get(index));
    }
  }

  public void delete(int index) {
    if (list.size() <= index) {
      System.out.println("Ошибка. Такого дела нет в списке");
    } else {
      System.out.println("Дело " + list.get(index) + " удалено");
      list.remove(index);
    }
  }

  public ArrayList<String> getTodos() {
    int i = 0;
    for (String item : list) {
      System.out.println(i + " " + item);
      i++;
    }
    return list;
  }

}