import java.util.Scanner;

public class Main {

  private static final String ADD_COMMAND = "add";
  private static final String EDIT_COMMAND = "edit";
  private static final String DELETE_COMMAND = "delete";
  private static final String LIST_COMMAND = "list";

  private static TodoList todoList = new TodoList();

  public static void main(String[] args) {

    System.out.println(
        "Добро пожаловать в todolist by Maxim.\nСписок команд:\n add - добавить дело;"
            + "\n edit N - заменить дело определенного номера, где N - номер дела;\n delete - удалить внесенное дело;"
            + "\n list - список всех дел.");
    Scanner scanner = new Scanner(System.in);
    int count = 0;
    while (true) {
      System.out.println("Введите команду:");
      String input = scanner.nextLine();
      String[] sentence = input.split("\\s+");
      String str = "";
      if (sentence[0].equals(ADD_COMMAND) || sentence[0].equals(EDIT_COMMAND) || sentence[0]
          .equals(DELETE_COMMAND)
          || sentence[0].equals(LIST_COMMAND)) {

        if (sentence[0].equals(ADD_COMMAND)) {
          if (sentence[1].matches("\\d+")) {
            int number = Integer.parseInt(sentence[1]);
            if (number > count) {
              for (int i = 2; i < sentence.length; i++) {
                str += sentence[i] + " ";
              }
              todoList.add(count + 1, str);
              count++;
            } else if (number <= count) {
              for (int i = 2; i < sentence.length; i++) {
                str += sentence[i] + " ";
              }
              todoList.add(number, str);
              count++;
            }
          } else {
            for (int i = 1; i < sentence.length; i++) {
              str += sentence[i] + " ";
            }
            todoList.add(str);
            count++;
          }
        }

        if (sentence[0].equals(EDIT_COMMAND)) {
          for (int i = 2; i < sentence.length; i++) {
            str += sentence[i] + " ";
          }
          int number = Integer.parseInt(sentence[1]);
          todoList.edit(str, number);
        }

        if (sentence[0].equals(DELETE_COMMAND)) {
          int number = Integer.parseInt(sentence[1]);
          todoList.delete(number);
          count--;
        }
        if (sentence[0].equals(LIST_COMMAND)) {
          todoList.getTodos();
        }
      } else {
        System.out.println(
            "Введенной команды не существует, пожалуста, введите корректную команду. (add, list, delete, edit)");
      }
    }
  }
}
