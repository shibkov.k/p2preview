public class Hospital {

    public static float[] generatePatientsTemperatures(int patientsCount) {

        //TODO: напишите метод генерации массива температур пациентов
        float[] temperatureData = new float[patientsCount];
        float temperature;
        for(int i = 0; i < patientsCount; i++) {
            temperature = (float) (32 + (Math.random() * 8));
            temperature = roundAvoid(temperature, 1);
            temperatureData[i] = temperature;
        }
        return temperatureData;
    }

    public static String getReport(float[] temperatureData) {
        /*
        TODO: Напишите код, который выводит среднюю температуру по больнице,количество здоровых пациентов,
            а также температуры всех пациентов.
        */
        StringBuilder allTemperatures = new StringBuilder();
        float totalTemperature = 0.0F;
        int healthyPatients = 0;
        for (int i = 0; i < temperatureData.length; i++) {
            if(i < temperatureData.length - 1) allTemperatures.
            append(temperatureData[i]).append(" ");
            else allTemperatures.append(temperatureData[i]);
            totalTemperature += temperatureData[i];
            if(temperatureData[i] >= 36.2F && temperatureData[i] <= 36.9F) healthyPatients++;
        }
        float averageTemperature = totalTemperature / temperatureData.length;
        averageTemperature = roundAvoid(averageTemperature, 2);

        String report =
                "Температуры пациентов: " + allTemperatures +
                        "\nСредняя температура: " + averageTemperature +
                        "\nКоличество здоровых: " + healthyPatients;

        return report;
    }

    public static float roundAvoid(float value, int places) {
        float scale = (float) Math.pow(10, places);
        return Math.round(value * scale) / scale;
    }
}
