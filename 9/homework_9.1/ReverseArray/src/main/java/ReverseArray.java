public class ReverseArray {

    //TODO: Напишите код, который меняет порядок расположения элементов внутри массива на обратный.
    public static String[] reverse (String[] strings){
        int ii = strings.length/2;
        for (int i  = 0; i < ii; i++ ){
            int end = strings.length - 1 - i;
            String tmp = strings[end];
            strings[end] = strings[i];
            strings[i] = tmp;
        }
        return strings;
    }
}
