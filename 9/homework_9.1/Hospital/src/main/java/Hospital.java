public class Hospital {

    public static float[] generatePatientsTemperatures(int patientsCount) {

        float [] temps = new float[patientsCount];
        //TODO: напишите метод генерации массива температур пациентов
        for(int i =0; i < patientsCount; i++){
            temps[i]  = (float) Math.random() * (40-32) + 32;
        }

        return temps;
    }

    public static String getReport(float[] temperatureData) {
        /*
        TODO: Напишите код, который выводит среднюю температуру по больнице,количество здоровых пациентов,
            а также температуры всех пациентов.
        */

        int patientsCount = temperatureData.length;
        float tempAverage = 0;
        int wealthPeople = 0;
        StringBuilder sb = new StringBuilder();
        for(int i =0; i < patientsCount; i++){
            tempAverage += temperatureData[i];
            sb.append(String.format("%.1f ",temperatureData[i]));
            if(temperatureData[i] >= 36.2F && temperatureData[i] <= 36.9F )
                wealthPeople++;
        }
        tempAverage /= patientsCount;

        String report =
                "Температуры пациентов: " + sb.toString().trim() +
                        "\nСредняя температура: " + String.format("%.2f",tempAverage) +
                        "\nКоличество здоровых: " + wealthPeople;

        return report;
    }
}
