import java.util.Arrays;

public class Main {
    public static void main(String[] args) {
        //Распечатайте сгенерированный в классе TwoDimensionalArray.java двумерный массив
        char [][] result = TwoDimensionalArray.getTwoDimensionalArray(3);
        System.out.println(Arrays.deepToString(result));
    }
}
