public class TwoDimensionalArray {
    public static char symbol = 'X';

    public static char[][] getTwoDimensionalArray(int size) {

        //TODO: Написать метод, который создаст двумерный массив char заданного размера.
        // массив должен содержать символ symbol по диагоналям, пример для size = 3
        // [X,  , X]
        // [ , X,  ]
        // [X,  , X]

        char[][] result = new char[size][size];
        for (int i = 0; i < size; i++) {
            for (int t = 0; t < size; t++) {
                if (i == t || size - 1 - i == t)
                    result[i][t] = 'X';
                else
                    result[i][t] = ' ';
            }
        }

        return result;
    }
}
