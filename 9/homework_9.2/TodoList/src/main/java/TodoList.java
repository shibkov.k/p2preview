import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;

public class TodoList {
    //    private ArrayList<String> list;
    private LinkedList<String> list;

    public TodoList() {
//        list = new ArrayList<>();
        list = new LinkedList<>();
    }

    public void add(String todo) {
        // TODO: добавьте переданное дело в конец списка
        list.add(todo);
    }

    public void add(int index, String todo) {
        // TODO: добавьте дело на указаный индекс,
        //  проверьте возможность добавления
        if (index < 0 || index > list.size())
            list.add(todo);
        else
            list.add(index, todo);
    }

    public void edit(String todo, int index) {
        // TODO: заменить дело на index переданным todo индекс,
        //  проверьте возможность изменения
        if (index < list.size()) {
            list.set(index, todo);
        }
    }

    public void delete(int index) {
        // TODO: удалить дело находящееся по переданному индексу,
        //  проверьте возможность удаления дела
        if (index < list.size()) {
            list.remove(index);
        }

    }

    public int todosAmount() {
        return list.size();
    }

    /*public ArrayList<String> getTodos() {
        // TODO: вернуть список дел
        return list;
    }*/

    public LinkedList<String> getTodos() {
        // TODO: вернуть список дел
        return list;
    }

    public void printAllTodos() {
        System.out.println("-------- TODOs ----------");
        int counter = 0;
        for (String s : list) {
            System.out.println(counter++ + " - " + s);
        }
    }

    public String getTodo(int index) {
        return list.get(index);
    }
}