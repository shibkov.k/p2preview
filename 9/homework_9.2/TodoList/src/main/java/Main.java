import java.util.ArrayList;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Main {
    private static TodoList todoList = new TodoList();

    public static void main(String[] args) {
        TodoList list = new TodoList();
        System.out.println("Your wish is my command?");
        Scanner scanner = new Scanner(System.in);
        Pattern pattern = Pattern.compile("(?i)(ADD|EDIT|LIST|DELETE|EXIT)(\\s+\\d+)?(\\s+\\w*)?");
        while (true) {
            String input = scanner.nextLine().trim();
            if (input.length() == 0)
                continue;
            Matcher matcher = pattern.matcher(input);
            ArrayList<String> tokens = new ArrayList<>();
            int index = -1;
            while (matcher.find()) {
                tokens.add(matcher.group(1).toUpperCase());
                if (matcher.group(2) != null && matcher.group(2).length() > 0)
                    index = Integer.parseInt(matcher.group(2).trim());
                tokens.add(matcher.group(3));
            }

            switch (tokens.get(0)) {
                case "ADD":
                    if (index == -1)
                        list.add(tokens.get(1));
                    else
                        list.add(index, tokens.get(1));
                    System.out.println("Добавлено " + tokens.get(1));
                    break;
                case "DELETE":
                    if (index != -1) {
                        System.out.println("Дело " + list.getTodo(index) + " удалено");
                        list.delete(index);
                    }
                    break;
                case "LIST":
                    list.printAllTodos();
                    break;
                case "EDIT":
                    if (index != -1) {
                        System.out.println("Дело "+ list.getTodo(index) +" заменено на " + tokens.get(1));
                        list.edit(tokens.get(1), index);
                    }
                    break;
                case "EXIT":
                    return;
            }
        }
    }
}
