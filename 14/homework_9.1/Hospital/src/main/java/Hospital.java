import java.text.DecimalFormat;

public class Hospital {

    public static float[] generatePatientsTemperatures(int patientsCount) {

        //TODO: напишите метод генерации массива температур пациентов
        final float MIN_TEMP = 36.60f;
        final float MAX_TEMP = 40.00f;

        float[] patientTemperatures = new float[patientsCount];

        for (int i = 0; i < patientsCount; i++) {
            patientTemperatures[i] = Math.round(Math.random() * (MAX_TEMP - MIN_TEMP) + MIN_TEMP);

        }
        return patientTemperatures;
    }

    public static String getReport(float[] temperatureData) {

        final float MIN_HEALTH = 36.2f;
        final float MAX_HEALTH = 36.9f;
        int healthPatient = 0;

        DecimalFormat formatter = new DecimalFormat("#0.00");
        float middleTemp = 0;

        String report = "Температуры пациентов: ";

        for (int i = 0; i < temperatureData.length; i++) {
            middleTemp += temperatureData[i];
            report += temperatureData[i] + " ";

            if (MIN_HEALTH <= temperatureData[i] && MAX_HEALTH >= temperatureData[i]) {
                healthPatient++;
            }
        }
        report = report.trim();
        middleTemp /= temperatureData.length;
        report += "\nСредняя температура: " + formatter.format(middleTemp) +
                "\nКоличество здоровых: " + healthPatient;

        /*
        TODO: Напишите код, который выводит среднюю температуру по больнице,количество здоровых пациентов,
            а также температуры всех пациентов.
        */

        return report;
    }
}
