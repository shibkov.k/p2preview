
public class ReverseArray {

    //TODO: Напишите код, который меняет порядок расположения элементов внутри массива на обратный.
    public static String[] reverse(String[] strings) {

        String localTemp; // временный пул для замены элементов массива

        for (int i = strings.length - 1; i >= strings.length / 2; i--) {

            localTemp = strings[i];
            strings[i] = strings[strings.length - i - 1];
            strings[strings.length - 1 - i] = localTemp;

        }


        return strings;
    }

}


