public class TwoDimensionalArray {

    public static char symbol = 'X';

    public static char[][] getTwoDimensionalArray(int size) {

        char[][] areaOfChar = new char[size][size];

        for (int i = 0; i < areaOfChar.length; i++) {
            for (int j = 0; j < areaOfChar[i].length; j++) {
                areaOfChar[i][j] = (i == j || j == areaOfChar.length - 1 - i) ? symbol : ' ';
            }
        }

        //TODO: Написать метод, который создаст двумерный массив char заданного размера.
        // массив должен содержать символ symbol по диагоналям, пример для size = 3
        // [X,  , X]
        // [ , X,  ]
        // [X,  , X]

        return areaOfChar;
    }
}
