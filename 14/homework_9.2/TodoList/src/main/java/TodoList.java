import java.util.ArrayList;

public class TodoList {

    protected final ArrayList<String> IMPORT_TODO = new ArrayList<>();

    public void add(String todo) {
        // TODO: добавьте переданное дело в конец списка

        IMPORT_TODO.add(todo);

    }

    public void add(int index, String todo) {
        // TODO: добавьте дело на указаный индекс,
        //  проверьте возможность добавления

        if (IMPORT_TODO.size() > index) {
            IMPORT_TODO.add(index, todo);
            System.out.println("добавили дело по указанный индекс");
        } else {
            IMPORT_TODO.add(todo);
            System.out.println("Указанный индекс не существует, дело добавленно в конец списка");
        }
    }

    public void edit(String todo, int index) {
        // TODO: заменить дело на index переданным todo индекс,
        //  проверьте возможность изменения

        if (IMPORT_TODO.size() > index) {
            IMPORT_TODO.set(index, todo);
            System.out.println("заменил дело по переданному индексу");
        }else
        {
            System.out.println("ничего не заменили по переданному индексу так как индекса не существует");
        }

    }

    public void delete(int index) {
        // TODO: удалить дело находящееся по переданному индексу,
        //  проверьте возможность удаления дела

        if (IMPORT_TODO.size() > index) {
            IMPORT_TODO.remove(index);
            System.out.println("Дело по индексу " + index + " удаленно");

        }else {
            System.out.println("не возможно удалить дело по переданному индексу так как индекса не существует");
        }

    }

    public ArrayList<String> getTodos() {
        // TODO: вернуть список дел

        return IMPORT_TODO;
    }

}