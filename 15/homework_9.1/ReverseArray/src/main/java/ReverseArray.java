public class ReverseArray {

    //TODO: Напишите код, который меняет порядок расположения элементов внутри массива на обратный.
    public static String[] reverse (String[] strings){
        String temporaryVar;
        int stringsLength = strings.length - 1;
        for (int i = 0; i < strings.length / 2; i++){
            temporaryVar = strings[stringsLength - i ];
            strings[stringsLength - i ] = strings[i];
            strings[i] = temporaryVar;
        }
        return strings;
    }
}
