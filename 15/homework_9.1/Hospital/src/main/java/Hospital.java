public class Hospital {

    public static final float MIN_TEMPERATURE = 32.0f;
    public static final float MAX_TEMPERATURE = 40.0f;
    public static final float MIN_TEMP_HEALTHY = 36.2f;
    public static final float MAX_TEMP_HEALTHY = 37f;

    public static float[] generatePatientsTemperatures(int patientsCount) {

        //TODO: напишите метод генерации массива температур пациентов
        float[] patientsTemperatures = new float[patientsCount];
        for (int i = 0; i < patientsCount; i++){
            double scale = Math.pow(10, 1);
            double result = Math.round(((Math.random() * (MAX_TEMPERATURE - MIN_TEMPERATURE) +
                    MIN_TEMPERATURE) * scale)) / scale;
            patientsTemperatures[i] = (float) result;
        }
        return patientsTemperatures;
    }

    public static String getReport(float[] temperatureData) {
        /*
        TODO: Напишите код, который выводит среднюю температуру по больнице,количество здоровых пациентов,
            а также температуры всех пациентов.
        */
        StringBuilder stringForReport = new StringBuilder();
        int numberOfHealthy = 0;
        float totalTemperature = 0;
        for (float nextTemperature : temperatureData){
            stringForReport = stringForReport.append(nextTemperature).append(" ");
            if (nextTemperature >= MIN_TEMP_HEALTHY && nextTemperature < MAX_TEMP_HEALTHY){
                numberOfHealthy = numberOfHealthy + 1;
            }
            totalTemperature = totalTemperature + nextTemperature;
        }
        stringForReport = stringForReport.deleteCharAt(stringForReport.length() - 1);//Это для теста, иначе не проходит
        float scale = (float) Math.pow(10, 2);
        float averageTemperature = (Math.round(totalTemperature / temperatureData.length * scale )) / scale;

        String report =
                "Температуры пациентов: " + stringForReport +
                        "\nСредняя температура: " + averageTemperature +
                        "\nКоличество здоровых: " + numberOfHealthy;

        return report;
    }
}
