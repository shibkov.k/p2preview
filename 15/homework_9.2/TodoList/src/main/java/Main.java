import java.util.ArrayList;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Main {
    private static final String regexOfInput = "^" + TodoCommands.getRegex();
    public static int numberTodo;
    public static TodoCommands nextAction;
    public static String nextTodo;

    private static TodoList todoList = new TodoList();

    public static void main(String[] args) {
        // TODO: написать консольное приложение для работы со списком дел todoList

        Scanner scanner = new Scanner(System.in);
        while (true) {
            String input = scanner.nextLine();
            if (input.equals("0")) {
                break;
            }
            input = input.trim();
            if (validateTodo(input)) {
                action();
            }
        }
    }

    public static boolean validateTodo(String input){
        Pattern pattern = Pattern.compile(regexOfInput);
        Matcher matcher = pattern.matcher(input);
        if (!matcher.find()){
            return false;
        }
        int lastIndex = matcher.end();
        String action = input.substring(matcher.start(), lastIndex);
        TodoCommands[] todoCommands =  TodoCommands.values();
        nextAction = null;
        for (int i = 0; i < TodoCommands.values().length; i++){
            if (action.equals(String.valueOf(todoCommands[i]))){
                nextAction = todoCommands[i];
            }
        }
        if (nextAction == null){
            return false;
        }
        input = input.substring(lastIndex).trim();
        String regex = "^([0-9]+[ ]*)";
        pattern = Pattern.compile(regex);
        matcher = pattern.matcher(input);
        if ((matcher.find())){
            numberTodo = Integer.parseInt((input.substring(matcher.start(), matcher.end() )).trim());
            lastIndex = matcher.end();
            input = input.substring(lastIndex).trim();
        }
        else {
            numberTodo = -1;
        }

        try {
            nextTodo = input;
        }
        catch (Exception e){
            nextTodo = "";
        }
        return true;
    }
    public static void action() {
        switch (nextAction) {
            case DELETE:
                todoList.delete(numberTodo);
                break;
            case LIST:
                ArrayList<String> toScreenArray = todoList.getTodos();
                for (int i = 0; i < toScreenArray.size(); i++) {
                    System.out.println(i + " - " + toScreenArray.get(i));
                }
                break;
            case ADD:
                todoList.add(numberTodo, nextTodo);
                break;
            case EDIT:
                todoList.edit(nextTodo, numberTodo);
                break;
        }
    }
}