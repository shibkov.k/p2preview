import java.util.ArrayList;

public class TodoList {
    private ArrayList<String> todoList;

    public TodoList() {
        this.todoList = new ArrayList<>();
    }

    private boolean indexValidator(int index){
        return (index >= 0 && index < todoList.size());
    }

    public void add(String todo) {
        //   TODO: добавьте переданное дело в конец списка
            todoList.add(todo);
    }

    public void add(int index, String todo) {
        // TODO: добавьте дело на указаный индекс,
        //  проверьте возможность добавления
        if (indexValidator(index)){
            todoList.add(index, todo);
        } else {
            this.add(todo);
        }
        System.out.println("Добавлено дело \"" + todo + "\"");
    }

    public void edit(String todo, int index) {
        // TODO: заменить дело на index переданным todo индекс,
        //  проверьте возможность изменения
        if (indexValidator(index)) {
            System.out.println("Дело " + todoList.get(index) + " заменено на :" + todo);
            todoList.set(index, todo);
        } else {
            System.out.println("Дела с таким номером не существует.");
        }
    }

    public void delete(int index) {
        // TODO: удалить дело находящееся по переданному индексу,
        //  проверьте возможность удаления дела
        if (indexValidator(index)){
            System.out.println("Дело \"" + todoList.get(index) + "\" удалено");
            todoList.remove(index);
        }
        else{
            System.out.println("Дела с таким номером не существует.");
        }
    }

    public ArrayList<String> getTodos() {
        // TODO: вернуть список дел
        ArrayList<String> toScreenArray = new ArrayList<>();
        toScreenArray.addAll(todoList);
        return toScreenArray;
    }
}