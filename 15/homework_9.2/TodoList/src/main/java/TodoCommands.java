import java.util.StringJoiner;

public enum TodoCommands {
    LIST,ADD,DELETE,EDIT;

    public static String getRegex() {
        return regex;
    }

    private static String regex;
    static {
        StringJoiner joiner = new StringJoiner("|");
        TodoCommands[] todoCommands = TodoCommands.values();
        for(int i = 0; i < TodoCommands.values().length; i++){
            joiner.add(String.valueOf(todoCommands[i]));
        }
        regex = joiner.toString();
    }
}
