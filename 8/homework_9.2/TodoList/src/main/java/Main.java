import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Main {
    private static TodoList todoList = new TodoList();

    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        while (true) {
            System.out.println("Введите команду:");
            String input = scanner.nextLine();

            String regexIndex = "(\\d+)\\s*";
            int index;
            int commandExclude = input.indexOf(" ") + 1;
            Pattern pattern = Pattern.compile(regexIndex);
            Matcher matcher = pattern.matcher(input);

            if (input.startsWith("ADD") && matcher.find()) {
                index = Integer.parseInt(matcher.group(1));
                input = input.substring(commandExclude);
                System.out.println("Добавлено дело " + todoList.add(index,input.substring((input.indexOf(" ") + 1))));
            } else if (input.startsWith("ADD")) {
                System.out.println("Добавлено дело " + todoList.add(input.substring(4)));
            } else if (input.startsWith("EDIT") && matcher.find()) {
                index = Integer.parseInt(matcher.group(1));
                input = input.substring(commandExclude);
                System.out.println("Дело " + todoList.edit(input.substring(input.indexOf(" ") + 1), index) +
                        " заменено на " + input.substring(input.indexOf(" ") + 1));
            } else if (input.startsWith("DELETE") && matcher.find()) {
                index = Integer.parseInt(matcher.group(1));
                System.out.println("Дело " + todoList.delete(index) + " удалено");
            } else if (input.startsWith("LIST")) {
                for (int i = 0; i < todoList.getTodos().size(); i++) {
                    System.out.println(i + " - " + todoList.getTodos().get(i));
                }
            } else if (input.equals("STOP")) {
                break;
            } else {
                System.out.println("ERROR");
            }
        }
    }
}