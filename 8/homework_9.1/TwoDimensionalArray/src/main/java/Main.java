public class Main {
    public static void main(String[] args) {
        //Распечатайте сгенерированный в классе TwoDimensionalArray.java двумерный массив

        char [][] size = TwoDimensionalArray.getTwoDimensionalArray(7);
        for (char[] j : size) {
            for (char i : j)
                System.out.print(" " + i);
            System.out.println();
        }
    }
}
