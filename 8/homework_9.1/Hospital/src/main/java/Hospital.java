public class Hospital {

    public static float[] generatePatientsTemperatures(int patientsCount) {
        int minTemperature = 32;
        int maxTemperature = 40;

        float[] getPatients = new float[patientsCount];
        for (int i = 0; i < getPatients.length; i++) {
            getPatients[i] = (float) (Math.random() * (maxTemperature - minTemperature)) + minTemperature;
        }
        return getPatients;
    }

    public static String getReport(float[] temperatureData) {

        float avrAllTemperature = 0.0f;
        int healthyPatients = 0;
        String report = "";
        float temperaturesToReport;
        String temperaturesToString = "";

        for (int i = 0; i <= temperatureData.length -1; i++) {
            avrAllTemperature += temperatureData[i] / (temperatureData.length);
            temperaturesToReport = roundFloatOneChar(temperatureData[i]);
            if(i != temperatureData.length - 1) {
                temperaturesToString += Float.toString(temperaturesToReport) + " ";
            } else {
                temperaturesToString += Float.toString(temperaturesToReport);
            }
            if (temperatureData[i] >= 36.2f && temperatureData[i] <= 36.9f) {
                healthyPatients++;
            }
            report =
                    "Температуры пациентов: " + temperaturesToString +
                            "\nСредняя температура: " + roundFloatTwoChar(avrAllTemperature) +
                            "\nКоличество здоровых: " + healthyPatients;
        }
        return report;
    }
    //public static float randomTemperature() {
        //int minTemperature = 32;
        //int maxTemperature = 40;

        //float randomTemperature = (float) (Math.random() * ((maxTemperature - minTemperature))) + minTemperature;
        //return randomTemperature;
    //}

    public static float roundFloatOneChar(float temperature) {
            float round = temperature * 10;

            round = (float) Math.round(round) / 10;
            return round;
    }
    public static float roundFloatTwoChar ( float temperature){
            float round = temperature * 100;

            round = (float) Math.round(round) / 100;
            return round;
    }
}