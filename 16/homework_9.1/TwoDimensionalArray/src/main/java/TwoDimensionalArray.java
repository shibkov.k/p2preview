public class TwoDimensionalArray {
    public static char symbol = 'X';

    public static char[][] getTwoDimensionalArray(int size) {
        char[][] tableX = new char[size][size];
        for (int i = 0; i < tableX.length; i++) {
            for (int j = 0; j < tableX[i].length; j++) {
                if ((j == i) || (j == tableX.length - 1 - i)) {
                    tableX[i][j] = 'X';
                } else {
                    tableX[i][j] = ' ';
                }

            }
        }
        return tableX;
    }
}
