import java.util.Arrays;

public class Hospital {
    private static final float LOWER_BOUND_TEMP = (float) 36.1;
    private static final float UPPER_BOUND_TEMP = 37;


    public static float[] generatePatientsTemperatures(int patientsCount) {
        float[] listTemperature = new float[patientsCount];
        for (int i = 0; i < listTemperature.length; i++) {
            float randomTemp = (float) (32.0 + ((Math.round(80 * Math.random())) / 10.0));
            listTemperature[i] = randomTemp;
        }
        return listTemperature;
    }

    public static String getReport(float[] temperatureData) {
        float sumTemperature = 0;
        float averTemperature = 0;
        int countHealthy = 0;
        String listAllPatientsTemperature = "";
        for (float temp : temperatureData) {
            listAllPatientsTemperature = listAllPatientsTemperature.concat(Float.toString(temp)).concat(" ");
            sumTemperature += temp;
            if ((temp > LOWER_BOUND_TEMP) && (temp < UPPER_BOUND_TEMP)) {
                countHealthy = countHealthy + 1;
            }
        }
        listAllPatientsTemperature = listAllPatientsTemperature.strip();
        if (temperatureData.length < 1) {
            averTemperature = 0;
        } else {
            averTemperature = (float) (Math.round(100 * sumTemperature / temperatureData.length)) / 100;
        }
        String report =
                "Температуры пациентов: " + listAllPatientsTemperature +
                        "\nСредняя температура: " + averTemperature +
                        "\nКоличество здоровых: " + countHealthy;
        return report;
    }
}
