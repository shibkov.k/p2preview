import java.util.Scanner;

public class Main {
    private static TodoList todoList = new TodoList();

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String input = "";
        while (!input.equalsIgnoreCase("exit")) {
            System.out.println("Введите команду ADD, LIST, EDIT, DELETE или EXIT");
            input = scanner.nextLine();
            String[] listInput = input.split("\\s+");
            String command = listInput[0];
            int size = listInput.length;
            switch (command) {
                case "ADD":
                    if (size > 1) {
                        int num = searchNumber(listInput[1]);
                        if (num < 0) {
                            todoList.add(concatArray(listInput, num));
                            break;
                        } else if (num > size - 1) {
                            wrongFormat(num);
                            break;
                        }
                        todoList.add(num,concatArray(listInput, num));
                        break;
                    }
                case "LIST":
                    todoList.getTodos().forEach(System.out::println);
                    break;
                case "EDIT":
                    if (size > 1) {
                        int num = searchNumber(listInput[1]);
                        if (num < 0) {
                            wrongFormat(listInput[0]);
                            break;
                        } else if (num > size - 1) {
                            wrongFormat(num);
                            break;
                        }
                        todoList.edit(concatArray(listInput, num),num);
                        break;
                    }
                case "DELETE":
                    if (size > 1) {
                        int num = searchNumber(listInput[1]);
                        if (num < 0) {
                            wrongFormat(listInput[0]);
                            break;
                        } else if (num > size - 1) {
                            wrongFormat(num);
                            break;
                        }
                        todoList.delete(num);
                    }
            }
        }
    }

    private static void wrongFormat(String command) {
        System.out.printf("Не правильный формат вввода команды \"%s\"%n", command);
    }

    private static void wrongFormat(int number) {
        System.out.printf("Дела с таким номером не существует %d%n", number);
    }

    private static int searchNumber(String command) {
        String[] peaceCommand = command.split("\\s+");
        String firstElement = peaceCommand[0];
        if (firstElement.matches("\\d+")) {
            return Integer.valueOf(firstElement);
        }
        return -1;
    }

    private static String concatArray(String[] listInput, int size) {
        StringBuilder result = new StringBuilder("");
        int start = size < 0 ? 1 : 2;
        for (int i = start; i < listInput.length; i++) {
            result.append(listInput[i]).append(" ");
        }
        return result.toString().trim();
    }

}
