import java.util.ArrayList;

public class TodoList {
    private ArrayList<String> todoList = new ArrayList<>();

    public void add(String todo) {
        todoList.add(todo);
        System.out.printf("Добавлено дело %s %n", todo);
    }

    public void add(int index, String todo) {
        if ((index >=0) && (index <= todoList.size())) {
            todoList.add(index, todo);
            System.out.printf("Добавлено дело %s в такое то место %d %n", todo, index);
        } else {
            add(todo);
            //System.out.printf("Добавлено дело %s в конец масива %n", todo);
        }
    }

    public void edit(String todo, int index) {
        if (index > 0 && index <= (todoList.size() - 1)) {
            todoList.set(index, todo);
            System.out.printf("Дело заменено %s в таком то месте %d %n", todo, index);
        } else {
            System.out.printf("Дело не может быть изменено, значение index:%d выходит за пределы массива %n", index);
        }
    }

    public void delete(int index) {
        if (index > 0 && index <= (todoList.size() - 1)) {
            todoList.remove(index);
            todoList.trimToSize();
            System.out.printf("Дело было удалено в таком то месте %d %n", index);
        } else {
            System.out.printf("Дело не может быть удалено, значение index:%d выходит за пределы массива %n", index);
        }
    }

    public ArrayList<String> getTodos() {
        return todoList;
    }


}