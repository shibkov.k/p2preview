public class ReverseArray {

    //TODO: Напишите код, который меняет порядок расположения элементов внутри массива на обратный.
    public static String[] reverse (String[] strings){
        String strTemp ="";
        int j = 0;
        for(int i = strings.length - 1; i > strings.length / 2; i--)
        {
            strTemp = strings[j];
            strings[j] = strings[i];
            strings[i] = strTemp;
            j++;
        }
        return strings;
    }
}
