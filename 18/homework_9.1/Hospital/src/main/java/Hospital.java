import java.util.Random;

public class Hospital {

    public static float[] generatePatientsTemperatures(int patientsCount) {

        //TODO: напишите метод генерации массива температур пациентов
        float[] PatientsTemp = new float[patientsCount];
        Random random = new Random();
        for(int i = 0; i < patientsCount; i++)
        {
            PatientsTemp[i] = 32 + random.nextInt(80) / 10.0f;
        }
        return PatientsTemp;
    }

    public static String getReport(float[] temperatureData) {
        /*
        TODO: Напишите код, который выводит среднюю температуру по больнице,количество здоровых пациентов,
            а также температуры всех пациентов.
        */

        String strPatientsTemp = "";
        float sumTemp = 0f;
        int cntHelth  = 0;
        for(int i = 0; i < temperatureData.length; i++)
        {
            if(!strPatientsTemp.isEmpty())
                strPatientsTemp += " ";
            strPatientsTemp += temperatureData[i];
            sumTemp += temperatureData[i];
            if(temperatureData[i] >= 36.2f && temperatureData[i] <= 36.9f)
                cntHelth++;
        }
        String report =
                "Температуры пациентов: " + strPatientsTemp +
                        "\nСредняя температура: " + Math.round(sumTemp / temperatureData.length * 100)/100.0 +
                        "\nКоличество здоровых: " + cntHelth;

        return report;
    }
}
