import java.util.ArrayList;

public class TodoList {
    ArrayList<String> todoList = new ArrayList<>();

    public void add(int index, String todo) {
        if (validateIndex(index)) {
            System.out.println("Добавлено дело \"" + todo.trim() + "\"" );
            todoList.add(index, todo);
        }
        // TODO: добавьте дело на указаный индекс,
        //  проверьте возможность добавления
    }

    public void add(String todo) {
        // TODO: добавьте переданное дело в конец списка
        add(todoList.size() , todo);
    }

    public void edit(String todo, int index) {
        if (validateIndex(index)) {
            System.out.println("Дело \"" + todoList.get(index) + "\" заменено на \"" + todo + "\'" );
            todoList.add(index + 1, todo);
            todoList.remove(index);
        }
        // TODO: заменить дело на index переданным todo индекс,
        //  проверьте возможность изменения
    }

    public void delete(int index) {
        if (validateIndex(index + 1)) {
            System.out.println("Дело \"" + todoList.get(index) + "\" удалено");
            todoList.remove(index);
        }
        // TODO: удалить дело находящееся по переданному индексу,
        //  проверьте возможность удаления дела
    }

    public void printList() {
        for (int i = 0; i < todoList.size(); i++) {
            System.out.println(i + " - " + todoList.get(i));
        }
    }

    public ArrayList<String> getTodos() {
        return todoList;

    }
    public boolean validateIndex(int index){
        if (todoList.size() >= index) {
            return true;
        }else{
            System.out.println("Дело с таким номером не существует");
            return false;
        }
    }

}