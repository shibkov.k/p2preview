import java.util.Scanner;
import java.util.regex.Pattern;

public class Main {
    private static TodoList todoList = new TodoList();

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        // TODO: написать консольное приложение для работы со списком дел todoList
        System.out.printf("\t\t\tДобро пожаловать в ваш ежедневник:\nДоступные команды для вас:\n" +
                "1 - команда add <Ваше дело> - добавляет дело \n2 - команда add <порядковый номер> <ваше дело> - добавляет дело по индексу" +
                "\n3 - команда edit <порядковый номер> <ваше дело> - изменяет записанное дело\n" +
                "4 - команда delete <порядковый номер> - удаляет дело\n5 - команда list - выводит список дел" +
                "Введите команду <stop> для завершение работы ежедневника\n\n");

        String comand;
        while (true) {
            comand = scanner.nextLine();
            if (comand.equalsIgnoreCase("stop")) {
                break;
            }
            WordProcessing(comand);

        }

    }
    public static void WordProcessing(String line){
        var addFind = Pattern.compile("add(?<text>.+)", Pattern.CASE_INSENSITIVE).matcher(line);
        var addIndexFind = Pattern.compile("add\\s(?<index>[0-9]+)\\s(?<text>.+)", Pattern.CASE_INSENSITIVE).matcher(line);
        var editIndexFind = Pattern.compile("edit+\\s(?<index>[0-9]+)\\s(?<text>.+)", Pattern.CASE_INSENSITIVE).matcher(line);
        var deleteIndexFind = Pattern.compile("delete\\s(?<index>[0-9]+)", Pattern.CASE_INSENSITIVE).matcher(line);
        if (addIndexFind.matches()) {
            todoList.add(Integer.parseInt(addIndexFind.group("index")), addIndexFind.group("text"));
        }else if (addFind.matches()) {
            todoList.add(addFind.group("text"));
        } else if (editIndexFind.matches()) {
            todoList.edit(editIndexFind.group("text"), Integer.parseInt(editIndexFind.group("index")));
        } else if (deleteIndexFind.matches()) {
           todoList.delete(Integer.parseInt(deleteIndexFind.group("index")));
        } else if (line.equalsIgnoreCase("list")) {
            todoList.printList();
        }else {
            System.err.println("Команда не распознана! обратите внимания на инструкцию");
        }
    }
}
