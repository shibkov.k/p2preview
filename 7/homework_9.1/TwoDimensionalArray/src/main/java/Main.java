public class Main {
    public static void main(String[] args) {
        //Распечатайте сгенерированный в классе TwoDimensionalArray.java двумерный массив
        char[][] list = TwoDimensionalArray.getTwoDimensionalArray(10);
        System.out.println(list.length);
        for (int i = 0; i < list.length; i++) {
            for (int j = 0; j < list.length; j++) {
                System.out.print(list[i][j]);
            }
            System.out.println();
        }
    }
}
