public class Hospital {
    private static final float PACIENT_TEMPERATURE_MIN = 32;
    private static final float PACIENT_TEMPERATURE_MAX = 40;
    private static final float PACIENT_TEMPERATURE_HEALTHY_MIN = (float) 35.2;
    private static final float PACIENT_TEMPERATURE_HEALTHY_MAX = (float) 36.9;


    public static float[] generatePatientsTemperatures(int patientsCount) {
        float[] patientTemperatureList = new float[patientsCount];
        for (int patient = 0; patient < patientTemperatureList.length; patient++) {
            patientTemperatureList[patient] = (float) (PACIENT_TEMPERATURE_MIN + Math.random() * (PACIENT_TEMPERATURE_MAX - PACIENT_TEMPERATURE_MIN));
        }
        //TODO: напишите метод генерации массива температур пациентов
        return patientTemperatureList;
    }

    public static String getReport(float[] temperatureData) {
        StringBuffer allTemperature = new StringBuffer();
        int numberOfHealthyPacient = 0;
        double averageTemperature = 0;
        for (double temperature : temperatureData) {
            averageTemperature += temperature;
            allTemperature.append(String.format(" %.1f", temperature));
            if (temperature > PACIENT_TEMPERATURE_HEALTHY_MIN && temperature < PACIENT_TEMPERATURE_HEALTHY_MAX) {
                numberOfHealthyPacient++;
            }
        }
        averageTemperature /= temperatureData.length;

        /*
        TODO: Напишите код, который выводит среднюю температуру по больнице,количество здоровых пациентов,
            а также температуры всех пациентов.
        */

        String report =
                "Температуры пациентов:" + allTemperature +
                        "\nСредняя температура: " + String.format("%.1f", averageTemperature) +
                        "\nКоличество здоровых: " + numberOfHealthyPacient;

        return report;
    }
}
