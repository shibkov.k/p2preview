public class Hospital {

  public static float minTemperature = 32.0f;
  public static float maxTemperature = 40.0f;

  public static float minHealthyTemperature = 36.2f;
  public static float maxHealthyTemperature = 36.9f;

  public static float[] generatePatientsTemperatures(int patientsCount) {

    float[] temperatureData = new float[patientsCount];

    for (int i = 0; i < patientsCount; i++) {
      temperatureData[i] =
          minTemperature + (float) (Math.random() * (maxTemperature - minTemperature));
    }

    return temperatureData;
  }

  public static String getReport(float[] temperatureData) {

    StringBuilder report = new StringBuilder("Температуры пациентов: ");
    float temperatureSum = 0.0f;
    int healthyAmount = 0;

    for (float temperature : temperatureData) {
      report.append(String.format("%.1f ", temperature));
      if (temperature >= minHealthyTemperature && temperature <= maxHealthyTemperature) {
        healthyAmount++;
      }
      temperatureSum += temperature;
    }

    report.deleteCharAt(report.length() - 1); // Удаляем лишний пробел в конце

    float averagetemperature =
        temperatureData.length > 0 ? temperatureSum / temperatureData.length : 0.0f;

    report.append(System.lineSeparator())
        .append(String.format("Средняя температура: %.1f", averagetemperature));
    report.append(System.lineSeparator())
        .append(String.format("Количество здоровых: %d", healthyAmount));

    return report.toString();
  }
}
