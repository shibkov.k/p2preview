public class Main {

  public static void main(String[] args) {

    float[] temperatureData = Hospital.generatePatientsTemperatures(10);
    System.out.println(Hospital.getReport(temperatureData));

    /* Math.round - округляет до целых значений
    Math.round(value * scale) / scale - не рекомендуется использовать для округления чисел,
    поскольку он усекает значение. Во многих случаях значения округляются неправильно!
    (с) Гугл
     */
  }
}
