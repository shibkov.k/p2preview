import java.util.ArrayList;

public class TodoList {

  private final ArrayList<String> todos = new ArrayList<>();
  private final static String INDEX_OUT_OF_BOUNDS_EXCEPTION_TEXT = "Дело с таким номером не существует";

  public ArrayList<String> getTodos() {
    return new ArrayList<>(todos);
  }

  /////////////////////////////////////////////////////////////////////////////////////////////////
  /////////////////////////////////////////////////////////////////////////////////////////////////
  // Методы для теста - тесты не обрабатывают Exception и падают, а я хочу Exception в Main =)
  public void add(String todo) {
    addTodo(todo);
  }

  public void add(int index, String todo) {
    addTodo(index, todo);
  }

  public void edit(String todo, int index) {
    try {
      editTodo(todo, index);

    } catch (Exception ex) {
      System.out.println(ex.getMessage());
      System.out.println("Если вы не тест, используйте \"addTodo\"");
    }
  }

  public void delete(int index) {
    try {
      deleteTodo(index);

    } catch (Exception ex) {
      System.out.println(ex.getMessage());
      System.out.println("Если вы не тест, используйте \"addTodo\"");
    }
  }
  /////////////////////////////////////////////////////////////////////////////////////////////////
  /////////////////////////////////////////////////////////////////////////////////////////////////

  public void addTodo(String todo) {
    todos.add(todo);
  }

  public void addTodo(int index, String todo) {
    if (index >= 0 && index < todos.size()) {
      todos.add(index, todo);

    } else {
      todos.add(todo);
    }
  }

  public void editTodo(String todo, int index) throws IndexOutOfBoundsException {
    if (isNumberTodoExists(index)) {
      todos.set(index, todo);

    } else {
      throw new IndexOutOfBoundsException(INDEX_OUT_OF_BOUNDS_EXCEPTION_TEXT);
    }
  }

  public void deleteTodo(int index) throws IndexOutOfBoundsException {
    if (isNumberTodoExists(index)) {
      todos.remove(index);
    } else {
      throw new IndexOutOfBoundsException(INDEX_OUT_OF_BOUNDS_EXCEPTION_TEXT);
    }
  }

  public String getTodoByNumber(int index) throws IndexOutOfBoundsException {
    if (isNumberTodoExists(index)) {
      return todos.get(index);
    } else {
      throw new IndexOutOfBoundsException(INDEX_OUT_OF_BOUNDS_EXCEPTION_TEXT);
    }
  }

  public boolean isNumberTodoExists(int index) {
    return index >= 0 && index < todos.size();
  }

  public String toStringAllTodos() {
    if (todos.isEmpty()) {
      return "Список дел пуст";

    } else {
      StringBuilder result = new StringBuilder();

      for (int i = 0; i < todos.size(); i++) {
        result.append(i + 1).append(String.format(" - %s%n", todos.get(i)));
      }

      return result.toString();
    }
  }
}