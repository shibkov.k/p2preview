import java.util.Scanner;

public class Main {

  private static final String WRONG_COMMAND_TEXT = "Ошибка ввода команды (используейте HELP)";
  private static TodoList todoList = new TodoList();

  public static void main(String[] args) {

    boolean programWorking = true;
    Scanner scanner = new Scanner(System.in);

    printInstructions();

    while (programWorking) {
      System.out.print("Введите команду: ");
      String[] commandParts = scanner.nextLine().split("\\W+", 3);
      String commandName = commandParts[0].toUpperCase();

      try {
        switch (commandName) {
          case "LIST":
            executeCommandList();
            break;

          case "ADD":
            executeCommandAdd(commandParts);
            break;

          case "EDIT":
            executeCommandEdit(commandParts);
            break;

          case "DELETE":
            executeCommandDelete(commandParts);
            break;

          case "CHECK":
            executeCommandCheck(commandParts);
            break;

          case "HELP":
            printInstructions();
            break;

          case "EXIT":
            programWorking = false;
            break;

          default:
            System.out.println(WRONG_COMMAND_TEXT);
        }

      } catch (Exception ex) {
        System.out.println(ex.getMessage());
      }
      System.out.println();
    }
  }

  private static void printInstructions() {
    System.out.println("Список команд для работы с TodoList: ");
    System.out.println();
    System.out.println("LIST: Выводит все дела с их порядковым номером");
    System.out.println(
        "ADD [N] description: Добавляет дело в список. Если указан N, например 'ADD 2 ...', то на указанную позицию, иначе в конец списка");
    System.out.println("EDIT N description: Заменяет дело N в списоке на новое");
    System.out.println("DELETE N: Удаляет дело с указанным номером");
    System.out.println("CHECK N: Проверить, если ли у вас дело под номером N");
    System.out.println("HELP: Вывод списка команд");
    System.out.println("EXIT: Выход из программы");
    System.out.println();
  }

  private static void executeCommandList() {
    System.out.println(todoList.toStringAllTodos());
  }

  private static void executeCommandAdd(String[] commandParts) throws Exception {
    if (commandParts[1].matches("\\d+")) {
      // Вторая часть команды число - нужно вставить новое дело на указанное место
      int index = getNumberFromCommandLine(commandParts[1]);

      todoList.addTodo(index - 1, commandParts[2]);
      System.out.printf("Добавлено дело \"%s\"%n", commandParts[2]);

    } else {
      String newTodoText = commandParts.length < 3 ? commandParts[1]
          : String.format("%s %s", commandParts[1], commandParts[2]);

      todoList.addTodo(newTodoText);
      System.out.printf("Добавлено дело \"%s\"%n", newTodoText);
    }
  }

  private static void executeCommandEdit(String[] commandParts) throws Exception {
    int index = getNumberFromCommandLine(commandParts[1]);
    String oldTodoText = todoList.getTodoByNumber(index - 1);

    todoList.editTodo(commandParts[2], index - 1);
    System.out.printf("Дело \"%s\" заменено на \"%s\"%n", oldTodoText, commandParts[2]);
  }

  private static void executeCommandDelete(String[] commandParts) throws Exception {
    int index = getNumberFromCommandLine(commandParts[1]);
    String oldTodoText = todoList.getTodoByNumber(index - 1);

    todoList.deleteTodo(index - 1);
    System.out.printf("Дело \"%s\" удалено%n", oldTodoText);
  }

  private static void executeCommandCheck(String[] commandParts) throws Exception {
    int index = getNumberFromCommandLine(commandParts[1]);

    if (todoList.isNumberTodoExists(index - 1)) {
      System.out.printf("Дело с номером %d: %s%n", index, todoList.getTodoByNumber(index - 1));

    } else {
      System.out.printf("Дело с номером %d не существует%n", index);
    }
  }

  private static int getNumberFromCommandLine(String text) throws Exception {
    try {
      return Integer.parseInt(text);

    } catch (Exception ex) {
      throw new Exception(WRONG_COMMAND_TEXT);
    }
  }
}
