public class CommandResult {

  public boolean hasError;
  public String messageText;

  public CommandResult() {
    this.hasError = false;
    this.messageText = "";
  }

  public CommandResult(boolean hasError, String messageText) {
    this.hasError = hasError;
    this.messageText = messageText;
  }

}
