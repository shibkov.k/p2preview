import java.text.DecimalFormat;
import java.util.Locale;

public class Hospital {

  public final static int PATIENTS = 30;
  public final static double MAX_TEMP = 40.0;
  public final static double MIN_TEMP = 32.0;
  public final static double MIN_TEMP_HEALTHY_PATIENT = 36.2;
  public final static double MAX_TEMP_HEALTHY_PATIENT = 36.9;

  public static float[] generatePatientsTemperatures(int patientsCount) {
    float[] patientsTemperatures = new float[patientsCount];
    for (int a = 0; a < patientsTemperatures.length; a++) {
      patientsTemperatures [a] = getPatientsTemperatures (32, 40);
       }
    return patientsTemperatures;
  }
public static float getPatientsTemperatures (double MIN_TEMP, double MAX_TEMP) {
    return  (float) ((Math.random() * (MAX_TEMP - MIN_TEMP)) +MIN_TEMP);
}

  public static String getReport(float[] temperatureData) {
        /*
        TODO: Напишите код, который выводит среднюю температуру по больнице,количество здоровых пациентов,
            а также температуры всех пациентов.
        */

    int healthyPatientsCount = 0;
    float sum = 0;
    String patientsTemperatures = "";

    for (float element : temperatureData) {
      patientsTemperatures += "\n" + String.format(Locale.ENGLISH, "%.1f", element);
      sum += element;
      if (element >= 36.2F && element <= 36.9F) {
        healthyPatientsCount++;
      }
    }
    String report =
        "Температуры пациентов: " + patientsTemperatures +
            "\nСредняя температура: " + String
            .format(Locale.ENGLISH, "%.2f", (sum / temperatureData.length)) +
            "\nКоличество здоровых: " + healthyPatientsCount;

    return report;
  }

  DecimalFormat formatter = new DecimalFormat("#0.00°C");
  double d = 37.67;

  {
    System.out.println(formatter.format(d)); //37.67°C
  }
}


