import java.util.ArrayList;

public class TodoList {

  private ArrayList<String> todoList = new ArrayList<>();

  public void add(String todo) {
    // TODO: добавьте переданное дело в конец списка
    System.out.println("Добавлено дело \"" + todo + "\"");
    todoList.add(todo);
  }

  public void add(int index, String todo) {
    // TODO: добавьте дело на указаный индекс,
    //  проверьте возможность добавления
    if (index > todoList.size()) {
      todoList.add(todo);
    }
    if (index <= todoList.size() && index >= 0) {
      System.out.println("Добавлено дело \"" + todo + "\" номер \"" + index + "\"");
      todoList.add(index, todo);
    }
  }


  public void edit(String todo, int index) {
    // TODO: заменить дело на index переданным todo индекс,
    //  проверьте возможность изменения
    if (index < todoList.size() && index >= 0) {
      System.out.println("Дело \"" + todoList.get(index) + "\" заменено на \"" + todo + "\"");
      todoList.set(index, todo);
    } else {
      System.out.println("Такого номера дела не существует ");
    }
  }

  public void delete(int index) {
    // TODO: удалить дело находящееся по переданному индексу,
    //  проверьте возможность удаления дела
    if (index < todoList.size() && index >= 0) {
      System.out.println("Дело \"" + todoList.get(index) + "\" удалено");
      todoList.remove(index);
    } else {
      System.out.println("Такого номера дела не существует");
    }
  }


  public ArrayList<String> getTodos() {
    // TODO: вернуть список дел
    return todoList;
  }

}