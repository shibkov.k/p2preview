public class Exer_5_1 {

        // ЗАДАНИЕ №2.
        // константы
    public static final int NUMBER_PATIENT = 30;  // константа-количество пациентов
    public static final double MINIMAL_TERM = 32;    // константа-минимальная температура пациентов
    public static final double MAXIMAL_TERM = 40;    // константа-максимальная температура пациентов
    public static final double MINIMAL_SCOPE = 36.2;    // константа-минимальная температура диапазона здоровых пациентов
    public static final double MAXIMAL_SCOPE = 36.9;    // константа-максимальная температура диапазона здоровых пациентов

    public static void main(String[] args) {

        // ЗАДАНИЕ №1.
        // Создайте массив из мнемонической фразы: Каждый охотник желает знать, где сидит фазан.
        // Напишите код, который меняет порядок расположения элементов внутри массива на обратный.

        String colorString = "Каждый охотник желает знать, где сидит фазан";
        String[] words = colorString.split("[^а-яА-Я]+");  //Создаём массив путём разбиения строки на элементы

        System.out.println("Прямой перебор:\n");

        for (int i = 0; i < words.length; i++) {  //Выводим в консоль эл-ты массива в прямом порядке
            System.out.println(words[i]);
        }

        System.out.println("==============================");

        System.out.println("Обратный перебор:\n");

        for (int re = (words.length - 1); re >= 0; re--) {  //Выводим в консоль эл-ты массива в обратном порядке
            System.out.println(words[re]);
        }

        System.out.println("==============================");

        System.out.println("Обратный перебор половины масива через временную переменную:\n");

        int n = words.length;  // Создаём переменную которая будет использоваться при обмене
        String tempVariable;

        for (int j = 0; j < n/2; j++) {  // Проходим циклом половину массива
            tempVariable = words[n - j - 1];
            words[n - j - 1] = words[j];
            words[j] = tempVariable;
        }

        for (int j = 0; j < words.length; j++) {  // Выводим результат в консоль
            System.out.println(words[j]);
        }

        // ЗАДАНИЕ №2.
        // Создайте массив типа float с температурами 30 пациентов (от 32 до 40 градусов).
        // Напишите код, который выводит среднюю температуру по больнице и количество здоровых
        // пациентов (с температурой от 36,2 до 36,9), а также температуры всех пациентов.
        // Вынесите в константы условия задания:
        // количество пациентов,
        // минимальная и максимальная температура пациентов,
        // минимальная и максимальная температура диапазона здоровых пациентов.

        Float[] hospital = new Float[NUMBER_PATIENT];  // Создаём новый объект-массив в классе Float

        float sum = 0;       // Суммарная температура
        int count = 0;      // Счётчик здоровых пациентов

        for (int i = 0; i < hospital.length; i++) {
            hospital[i] = (float)(MINIMAL_TERM + (MAXIMAL_TERM - MINIMAL_TERM) * Math.random());  // Генератор температур

            sum += hospital[i];

            if (hospital[i] < MAXIMAL_SCOPE && hospital[i] > MINIMAL_SCOPE) {
                count++;
            }
        }

        for (int i = 0; i < hospital.length; i++) {
            System.out.println("patient № "+ (i+1) + "|\t" + hospital[i] + "\tdegrees");
        }

        System.out.println("Average temperature of patient " + (sum / NUMBER_PATIENT) + " degrees");
        System.out.println("Sum of healthy " + count);


        // ЗАДАНИЕ №3*.
        //Создайте с помощью циклов двумерный массив строк. При его распечатке в консоли должен
        // выводиться крестик из X.

        String[] [] xPath = new String[7] [7];   // Создаём массив в классе String

        for (int i = 0; i < xPath.length; i++) {
            for (int j = 0; j <xPath[i].length; j++) {
                xPath[i] [j] = " ";    // Заполним массив пробелами

                if (i == j || (i + j) == (xPath.length - 1) ) {
                    xPath[i] [j] = "X";  // Заменим пробелы на Х в диагоналях
                }
            }
        }

        for (int i = 0; i < xPath.length; i++) {   //  Выводим массив в консоль
            System.out.println();
            for (int j = 0; j < xPath[i].length; j++) {
                System.out.print(xPath[i] [j]);
            }
        }
    }
    
}
