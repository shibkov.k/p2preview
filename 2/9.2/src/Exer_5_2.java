import java.util.ArrayList;
import java.util.Scanner;

public class Exer_5_2 {
    public static Scanner in = new Scanner(System.in);

    public static void main(String[] args) {

        // ЗАДАНИЕ №1.
        //Разработайте список дел, который управляется командами в консоли.
        // Команды: LIST, ADD, EDIT, DELETE.

        ArrayList<String> planner = new ArrayList<>();  // Создаём новый список

        int index = -1;  // Создаём переменную индекс и инициализируем её

        System.out.println("  =========== Список команд ===========\n" + "LIST - выводит дела с их порядковыми номерами;\n" +
                "ADD - добавляет дело в конец списка;\n" + "ADD (число) - добавляет дело в указанный пункт;\n" +
                "EDIT (число) - заменяет дело с указанным номером;\n" + "DELETE (число) - удаляет дело из списка;\n" +
                "EXIT - выход\n" + "  ======================================");

        while (true) {               // цикл для прокрутки меню

            System.out.print("Введите команду меню:\t");
            String enterCommand = in.nextLine();

            String[] nameCommand = enterCommand.split("\\s+", 3); // Разбиваем строку на три части и извлекаем подстроки

            String command = nameCommand[0]; // Присваеваем переменной значение равное подстроке с нулевым индексом
            int countWork = 0;               // Номер индекса в списке

            if (nameCommand[0].equals("ADD")) {

                if (nameCommand[1].matches("\\d+")) { // Извлекаем значение из подстроки, если подстрока - цифра.
                    index = Integer.parseInt(nameCommand[1]);    // Переменная индекс равна значению, извлеченному из подстроки

                    if (index != -1) {
                        planner.add(index, nameCommand[2]);
                    }
                }

                else planner.add(nameCommand[1]);             // Эта ветка добавляет дело в конец списка, если индекс в команде отсутствует

            }

            else if (nameCommand [0].equals("LIST")) {  // Условие вывода списка в консоль - команда LIST
                for (String output : planner) {
                    System.out.println(countWork + "\t" + output);
                    countWork++;
                }
            }

            else if (nameCommand[0].equals("EDIT")) {
                if (nameCommand[1].matches("\\d+")) {  // Извлекаем значение из подстроки, если подстрока - цифра.
                    index = Integer.parseInt(nameCommand[1]);    // Переменная индекс равна значению, извлеченному из подстроки

                    planner.set(index, nameCommand[2]);      // Метод изменяющий название задачи
                }

            }

            else if (nameCommand[0].equals("DELETE")) {
                if (nameCommand[1].matches("\\d+") ) {  // Извлекаем значение из подстроки, если подстрока - цифра.
                    index = Integer.parseInt(nameCommand[1]);    // Переменная индекс равна значению, извлеченному из подстроки

                    planner.remove(index);      // Метод удаляющий задачу по номеру пункта
                }

            }

            else if (command.equals("EXIT")) {
                break;
            }
            else System.out.println("Введите корректную команду!");
        }

    }

}

