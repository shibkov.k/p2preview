import java.util.ArrayList;

public class TodoList {

    ArrayList<String> todoList = new ArrayList<>();

    public void add(String todo) {
        todoList.add(todo);
    }

    public void add(int index, String todo) {
        if (todoList.size() < index) {
            todoList.add(todo);
        } else {
            todoList.add(index, todo);
        }
    }

    public void edit(String todo, int index) {
        if (todoList.size() > index) {
            todoList.remove(index);
            todoList.add(index, todo);
        }
    }

    public void delete(int index) {
        if (todoList.size() > index) {
            todoList.remove(index);
        }
    }

    public ArrayList<String> getTodos() {
        return new ArrayList<>(todoList);
    }

}