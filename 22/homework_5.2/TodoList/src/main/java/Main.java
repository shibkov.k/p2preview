import java.util.Scanner;

public class Main {
    private static final TodoList todoList = new TodoList();

    public static void main(String[] args) {

        int index;
        String todo;
        while (true) {
            Scanner scanner = new Scanner(System.in);
            String input = scanner.nextLine();
            if (input.equals("EXIT")) {
                System.out.println("До свидания!Успехов в ваших делах!");
                break;
            }
            String[] partsString = input.split(" ", 3);
            switch (partsString[0]) {
                case "ADD":
                    if (partsString[1].matches("\\d+")) {
                        index = Integer.parseInt(partsString[1]);
                        todo = " " + partsString[2];
                        if (index >= todoList.getTodos().size()) {
                            todoList.add(todo);
                        } else {
                            todoList.add(index, todo);
                        }
                    } else {
                        todo = input.substring(input.indexOf(" "));
                        todoList.add(todo);
                    }
                    System.out.println("Добавлено дело:" + todo);
                    break;
                case "EDIT":
                    index = Integer.parseInt(partsString[1]);
                    if (index >= todoList.getTodos().size()) {
                        System.out.println("Дела с таким номером не существует");
                    } else {
                        todo = " " + partsString[2];
                        System.out.println("Дело:" + todoList.getTodos().get(index) + " изменено на:" + todo);
                        todoList.edit(todo, index);
                    }
                    break;
                case "REMOVE":
                    index = Integer.parseInt(partsString[1]);
                    if (index >= todoList.getTodos().size()) {
                        System.out.println("Дела с таким номером не существует");
                    } else {
                        System.out.println("Дело:" + todoList.getTodos().get(index) + " удалено");
                        todoList.delete(index);
                    }
                    break;
                case "LIST":
                    for (int i = 0; i < todoList.getTodos().size(); i++) {
                        System.out.println(i + " -" + todoList.getTodos().get(i));
                    }
                    break;
                default:
                    System.out.println("Неверная команда,введите:" + System.lineSeparator() +
                            "\tADD - для добавления дела." + System.lineSeparator() +
                            "\tEDIT - для редактирования дела." + System.lineSeparator() +
                            "\tREMOVE - для удаления дела." + System.lineSeparator() +
                            "\tLIST - для просмотра списка дел." + System.lineSeparator() +
                            "\tEXIT - для выхода.");
                    break;
            }
        }
    }
}
