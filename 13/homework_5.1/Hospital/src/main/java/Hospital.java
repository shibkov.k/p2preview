import java.util.Arrays;

public class Hospital {

    final static float MIN_TEMPERATURE = 32f;
    final static float MAX_TEMPERATURE = 40f;
    final static float MIN_TEMPERATURE_OF_HEALTHY_PATIENTS = 36.2f;
    final static float MAX_TEMPERATURE_OF_HEALTHY_PATIENTS = 36.9f;

    public static float[] generatePatientsTemperatures(int patientsCount) {

        //TODO: напишите метод генерации массива температур пациентов
         float[] patientsTemperatures = new float[patientsCount];

        for(int i = 0; i < patientsCount; i++){
            patientsTemperatures[i] = (float) Math.round((MIN_TEMPERATURE + (MAX_TEMPERATURE - MIN_TEMPERATURE)
                    * Math.random()) * 10)/10;
        }
        return patientsTemperatures;
    }

    public static String getReport(float[] temperatureData) {
        /*
        TODO: Напишите код, который выводит среднюю температуру по больнице,количество здоровых пациентов,
            а также температуры всех пациентов.
        */
        int countHealthy = 0;
        float sumTemperature = 0f;
        float averageTemperature;
        for(int i = 0; i < temperatureData.length; i++){
            if(temperatureData[i] >= MIN_TEMPERATURE_OF_HEALTHY_PATIENTS &&
               temperatureData[i] <= MAX_TEMPERATURE_OF_HEALTHY_PATIENTS){
               countHealthy += 1;
            }
            sumTemperature += temperatureData[i];
        }
        averageTemperature = (float) Math.round(sumTemperature / temperatureData.length * 100)/100;

        String toArrays = Arrays.toString(temperatureData).replaceAll("[\\[\\]]", "").replaceAll(",", "");

        String report =
                "Температуры пациентов: " + toArrays +
                "\nСредняя температура: " + averageTemperature +
                "\nКоличество здоровых: " + countHealthy;

        return report;
    }
}
