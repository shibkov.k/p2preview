public class TwoDimensionalArray {
    public static char symbol = 'X';

    public static char[][] getTwoDimensionalArray(int size) {

        //TODO: Написать метод, который создаст двумерный массив char заданного размера.
        // массив должен содержать символ symbol по диагоналям, пример для size = 3
        // [X,  , X]
        // [ , X,  ]
        // [X,  , X]

        // Задаём симметричный массив
        char[][] crossX = new char[size][size];
        // Выводим массив в консоль
        for (int i = 0; i < crossX.length; i++){
            for (int j = 0; j < crossX[i].length; j++){
                crossX[i][j] = ' ';                          // Все элементы заполняем пробелами
                crossX[i][i] = symbol;                       // Элементы с одинаковыми индексами заполняем "Х" (диагональ)
                crossX[i][crossX.length - (i + 1)] = symbol; // Элементы с индексами второй диагонали тоже - "Х"
                System.out.print(crossX[i][j]);
            }
            System.out.println();
        }

        return crossX;
    }
}
