public class Main {

    public static void main(String[] args) {
        String line = "Каждый охотник желает знать, где сидит фазан";
        String[] lineStrings = line.split(",?\\s+");

        ReverseArray.reverse(lineStrings);

        for (String lineWords : lineStrings) {
            System.out.println(lineWords);
        }
    }
}
