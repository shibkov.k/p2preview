import java.util.Scanner;

public class Main {
    private static TodoList todoList = new TodoList();

    public static void main(String[] args) {
        // TODO: написать консольное приложение для работы со списком дел todoList

        System.out.println("Управление делами с помощью команд: LIST, ADD, EDIT, DELETE. <<КОМАНДА № ДЕЛО>>");

        for(int indexInt = 0; ;){
            Scanner scanner = new Scanner(System.in);
            System.out.print("Enter command: ");
            String commandUser = scanner.nextLine();
            String index = commandUser.replaceAll("[^0-9]","");       //Number(str) business

            if(!index.isEmpty()){
                indexInt = Integer.parseInt(index);                                  //Number(int) business
            }

            int index1Space = commandUser.indexOf(" ");
            String usersBusiness;
            if (index1Space <= 0){
                usersBusiness = commandUser;
            } else {
                String usersBusinessWithNumber = commandUser.substring(index1Space);
                usersBusiness = usersBusinessWithNumber.replaceAll("[0-9]", "").trim(); //Business
            }

            if (commandUser.contains("LIST")) {
                todoList.getTodos();
            }

            if (commandUser.contains("ADD") && !index.isEmpty()){
                todoList.add(indexInt, usersBusiness);
            }

            if (commandUser.contains("ADD") && index.isEmpty()){
                todoList.add(usersBusiness);
            }

            if (commandUser.contains("DELETE")) {
                todoList.delete(indexInt);
            }

            if (commandUser.contains("EDIT")) {
                todoList.edit(indexInt, usersBusiness);
            }
        }
    }
}
