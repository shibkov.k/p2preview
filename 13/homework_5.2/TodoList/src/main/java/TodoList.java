import java.util.ArrayList;

public class TodoList {

    ArrayList<String> todoLists = new ArrayList<>();

    public void add(String todo) {
        // TODO: добавьте переданное дело в конец списка
        todoLists.add(todo);
        System.out.println("Добавлено дело: " + todo);
    }

    public void add(int index, String todo) {
        // TODO: добавьте дело на указаный индекс,
        //  проверьте возможность добавления
        if (index < todoLists.size()){
            todoLists.add(index, todo);
        } else {
            todoLists.add(todo);
        }
    }

    public void edit(int index, String todo) {
        // TODO: заменить дело на index переданным todo индекс,
        //  проверьте возможность изменения
        if (index >= todoLists.size()){
            System.out.println("Дело с таким номером не существует!");
        } else {
            System.out.println("Дело " + todoLists.get(index) + " заменено на " + todo);
            todoLists.remove(index);
            todoLists.add(index, todo);
        }
    }

    public void delete(int index) {
        // TODO: удалить дело находящееся по переданному индексу,
        //  проверьте возможность удаления дела
        if (index >= todoLists.size()){
            System.out.println("Дело с таким номером не существует!");
        } else {
            todoLists.remove(index);
        }
    }

    public ArrayList<String> getTodos() {
        // TODO: вернуть список дел
        System.out.println("Список дел: ");
        for (int i = 0; i < todoLists.size(); i++) {
            System.out.println(i + " - " + todoLists.get(i));
        }
        return todoLists;
    }

}