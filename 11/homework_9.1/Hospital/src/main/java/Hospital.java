public class Hospital {

    public static float[] generatePatientsTemperatures(int patientsCount) {


        //TODO: напишите метод генерации массива температур пациентов

        float [] patientTemp = new float[patientsCount];
        final int MIN_TEMP_GENERATE = 32;
        final int MAX_TEMP_GENERATE = 40;
        float tempRandomGenerate = (float) ((Math.random() * (MAX_TEMP_GENERATE-MIN_TEMP_GENERATE)) + MIN_TEMP_GENERATE);


        for (int i = 0; i < patientTemp.length; i++) {
            patientTemp[i] = tempRandomGenerate;
        }
        return patientTemp;
    }

    public static String getReport(float[] temperatureData) {
        /*
        TODO: Напишите код, который выводит среднюю температуру по больнице,количество здоровых пациентов,
            а также температуры всех пациентов.
        */
        final float MAX_TEMP_HEAL = 36.9f;
        final float MIN_TEMP_HEAL = 36.2f;

        float sumTemp = 0;
        int pacientHeal = 0;
        String mediumTemperature;
        String tempPrint = "";
        for (float elements : temperatureData) {
            sumTemp += elements;
            if (elements <= MAX_TEMP_HEAL && elements >= MIN_TEMP_HEAL )
            {
                pacientHeal++;
            }
            tempPrint +=  elements + " ";
        }

        mediumTemperature = String.format("%.2f",sumTemp / temperatureData.length);
        tempPrint = tempPrint.trim();


        String report =
                "Температуры пациентов: " + tempPrint +
                        "\nСредняя температура: " + mediumTemperature +
                        "\nКоличество здоровых: " + pacientHeal
                ;

        return report;
    }
}
