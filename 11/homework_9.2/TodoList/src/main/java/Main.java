import java.util.Scanner;

public class Main {
    private static TodoList todoList = new TodoList();

    public static void main(String[] args) {
        // TODO: написать консольное приложение для работы со списком дел todoList

        Scanner scanner = new Scanner(System.in);
        TodoList todoList = new TodoList();
        while (true) {
            String input = scanner.nextLine();
            if (input.equals("0")) {
                break;
            }
            System.out.println(input);


            String [] userSplit = input.split(" ");


            // смотрим на второй индекс
            int indexSecondSymb = -1;
            if (userSplit.length > 1 && userSplit[1].matches("\\d")){
                indexSecondSymb = Integer.parseInt(userSplit[1]);
            }

            switch (userSplit[0]){
                case("ADD"):
                    input = input.replaceFirst("ADD","").trim();
                    if (indexSecondSymb > 0 && indexSecondSymb < todoList.getTodos().size()) {
                        todoList.add(indexSecondSymb,input.replaceFirst("\\d","").trim());
                    }
                    else todoList.add(input.strip());
                    break;

                case("DELETE"):
                    todoList.delete(indexSecondSymb);
                    break;

                case("EDIT"):
                    input = input.replaceFirst("EDIT","").replaceFirst("\\d","").trim();
                    todoList.edit(input,indexSecondSymb);
                    break;

                case("LIST"):
                    for (int i = 0; i < todoList.getTodos().size(); i++) {
                        System.out.println(i + " - " + todoList.getTodos().get(i));
                    }
                    break;
                }
            }
        }
}
