import java.util.ArrayList;

public class TodoList {

    ArrayList list = new ArrayList();

    public void add(String todo) {
        // TODO: добавьте переданное дело в конец списка
        list.add(todo);
    }

    public void add(int index, String todo) {
        // TODO: добавьте дело на указаный индекс,
        //  проверьте возможность добавления
        list.trimToSize();
        if (list.size() >= index){
            list.add(index,todo);
        } else {
            add(todo);
        }

    }

    public void edit(String todo, int index) {
        // TODO: заменить дело на index переданным todo индекс,
        //  проверьте возможность изменения
        list.trimToSize();
        if (list.size() >= index){
            list.set(index,todo);
        }
    }

    public void delete(int index) {
        // TODO: удалить дело находящееся по переданному индексу,
        //  проверьте возможность удаления дела
        list.trimToSize();
        if (list.isEmpty() || index >= list.size()){
            System.out.println("Дело с таким номером не существует");
        } else {
            list.remove(index);
        }
    }

    public ArrayList<String> getTodos() {
        // TODO: вернуть список дел
        return list;
    }

}