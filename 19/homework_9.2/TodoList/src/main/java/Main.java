import java.util.Scanner;

public class Main {

  private static TodoList todoList = new TodoList();

  public static void main(String[] args) {
    System.out.println("Введите дело: ");
    Scanner scanner = new Scanner(System.in);

    while (true) {
      String input = scanner.nextLine();
      String[] afterSplit = input.split(" ", 2);
      String command = afterSplit[0];

      switch (command) {
        case "ADD":
          boolean hasDigit = input.matches(".*\\d+.*");
          if (hasDigit) {
            String[] afterSplitIfHasDigit = input.split(" ", 3);
            String index = afterSplitIfHasDigit[1];
            String newToDo = afterSplitIfHasDigit[2];
            todoList.add(Integer.parseInt(index), newToDo);
          } else {
            todoList.add(afterSplit[1]);
          }

          break;

        case "LIST":
          System.out.println(todoList.getTodos());
          break;

        case "EDIT":
          String[] afterSplitEditCase = input.split(" ", 3);
          String index = afterSplitEditCase[1];
          String newToDo = afterSplitEditCase[2];
          todoList.edit(newToDo, Integer.parseInt(index));
          break;

        case "DELETE":
          String indexToDelete = afterSplit[1];
          todoList.delete(Integer.parseInt(indexToDelete));
          break;

        default:

          break;
      }
    }
  }
}




