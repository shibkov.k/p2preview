import java.util.ArrayList;

public class TodoList {
    ArrayList<String> todoList = new ArrayList<>();

    public void add(String todo) {

        todoList.add(todo);
    }

    public void add(int index, String todo) {

        if(index > todoList.size()) {
            todoList.add(todo);
        }else {
            todoList.add(index,todo);
        }
    }

    public void edit(String todo, int index) {

        if(index <= todoList.size()) {
            todoList.remove(index);
        }
    }

    public void delete(int index) {

        if(index <= todoList.size()) {
            todoList.remove(index);
        }
    }

    public ArrayList<String> getTodos() {

        return todoList;
    }

}