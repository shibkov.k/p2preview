import java.util.Locale;

public class Hospital {

  public final static int PATIENTS = 30;
  public final static double MAX_TEMP = 40.0;
  public final static double MIN_TEMP = 32.0;
  public final static float MIN_TEMP_HEALTH_PATIENT = (float) 36.2;
  public final static float MAX_TEMP_HEALTH_PATIENT = (float) 36.9;

  public static float[] generatePatientsTemperatures(int patientsCount) {

    float[] patientsTemperatures = new float[patientsCount];

    for (int i = 0; i < patientsTemperatures.length; i++) {
      patientsTemperatures[i] = (float) (Math.random() * (MAX_TEMP - MIN_TEMP)) + 32;
    }

    return patientsTemperatures;
  }

  public static String getReport(float[] temperatureData) {

    int temperatureGoodPatients = 0;
    float temperatureSum = 0;
    String patientTemperatures = "";

    for (float element : temperatureData) {
      patientTemperatures += String.format(Locale.US, "%.1f", element) + " ";
      temperatureSum += element;

      if ((element >= MIN_TEMP_HEALTH_PATIENT) && (element
          <= MAX_TEMP_HEALTH_PATIENT)) {
        temperatureGoodPatients++;
      }

    }

    String report =
        "Температуры пациентов: " + patientTemperatures.trim() +
            "\nСредняя температура: " + String.format(Locale.US,"%.2f",
            temperatureSum / temperatureData.length) +
            "\nКоличество здоровых: " + temperatureGoodPatients;

    return report;
  }
}
