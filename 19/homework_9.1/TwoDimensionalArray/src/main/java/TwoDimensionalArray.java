public class TwoDimensionalArray {

  public static char symbol = 'X';


  public static char[][] getTwoDimensionalArray(int size) {

    char[][] countX = new char[size][size];

    for (int i = 0; i < countX.length; i++) {
      for (int j = 0; j < countX[i].length; j++) {
        countX[i][j] = i + j == size - 1 || i == j ? symbol : ' ';

        System.out.println(countX[i][j]);

      }

    }

    return countX;
  }
}

