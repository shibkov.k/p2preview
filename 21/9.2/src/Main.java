import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Main {

    private final static BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

    public static void main(String[] args) throws IOException {

        ToDoList toDoList = new ToDoList();

        while (true) {

            System.out.println("Введите команду или добавьте дело: ");
            String console = reader.readLine();

            if (toDoList.addMatcherWithoutIndex(console)) {
                toDoList.addToList(console);

            } else if (toDoList.addMatcherWithIndex(console)) {
                toDoList.addToListWithIndex(console);

            } else if (toDoList.listMatcher(console)) {
                toDoList.getList(console);

            } else if (toDoList.editMatcher(console)) {
                toDoList.editList(console);

            } else if (toDoList.deleteMatcher(console)) {
                toDoList.deleteFromList(console);

            } else if (console.equalsIgnoreCase("stop")) {
                break;
            } else {
                System.out.println("Неверная команда, попробуйте еще раз");
            }
        }
        reader.close();

        toDoList.printList();
    }
}
