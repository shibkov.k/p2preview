import java.util.LinkedList;
import java.util.List;

public class ToDoList {

    private List<String> toDoList = new LinkedList<>();

    public boolean addMatcherWithoutIndex(String string) {
        return string.matches("^(a?A?d?D?D?d?)\\s[^\\d].*");
    }

    public boolean addMatcherWithIndex(String string) {
        return string.matches("^(a?A?d?D?D?d?)\\s[\\d]+\\s.*");
    }

    public boolean listMatcher(String string) {
        return string.equalsIgnoreCase("list");
    }

    public boolean editMatcher(String string) {
        return string.matches("(E?e?d?D?I?i?t?T?)\\s\\d+.*");
    }

    public boolean deleteMatcher(String string) {
        return string.matches("(DELETE)?(delete)?\\s\\d.*");
    }

    public void addToList(String console) {
        if (addMatcherWithoutIndex(console)) {
            String[] split = console.split("\\s", 2);
            toDoList.add(split[1]);
        }
    }

    public void addToListWithIndex(String console) {
        if (addMatcherWithIndex(console)) {
            String[] split = console.split("\\s", 3);
            int index = Integer.parseInt(split[1]);
            if (index >= split.length) {
                System.out.println("Неверный индекс");
                return;
            }
            toDoList.add(index, split[2]);
        }
    }

    public void getList(String console) {
        if (console.equalsIgnoreCase("list")) {
            printList();
        }
    }

    public void editList(String console) {
        if (editMatcher(console)) {
            String[] split = console.split("\\s", 3);
            int index = Integer.parseInt(split[1]);
            if (index >= split.length) {
                System.out.println("Неверный индекс");
                return;
            }
            toDoList.set(index, split[2]);
        }
    }

    public void deleteFromList(String console) {
        if (deleteMatcher(console)) {
            String[] split = console.split("\\s", 3);
            int index = Integer.parseInt(split[1]);
            if (index >= split.length) {
                System.out.println("Неверный индекс");
                return;
            }
            toDoList.remove(index);
        }
    }

    public void printList() {
        for (int i = 0; i < toDoList.size(); i++) {
            System.out.println((i + 1) + ": " + toDoList.get(i));
        }
    }
}
